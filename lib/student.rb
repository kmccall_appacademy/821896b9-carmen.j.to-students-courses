# ## Student
# * `Student#initialize` should take a first and last name.
# * `Student#name` should return the concatenation of the student's
#   first and last name.
# * `Student#courses` should return a list of the `Course`s in which
#   the student is enrolled.
# * `Student#enroll` should take a `Course` object, add it to the
#   student's list of courses, and update the `Course`'s list of
#   enrolled students.
#     * `enroll` should ignore attempts to re-enroll a student.
# * `Student#course_load` should return a hash of departments to # of
#   credits the student is taking in that department.
# * Update `Student#enroll` so that you raise an error if a `Student`
#   enrolls in a new course that conflicts with an existing course time.
#     * May want to write a `Student#has_conflict?` method to help.
require_relative 'course'

class Student
    attr_reader :first_name, :last_name
    attr_accessor :courses

    def initialize(first, last)
        @first_name = first
        @last_name = last
        @courses = []
    end

    def name
        "#{@first_name} #{@last_name}"
    end

    def enroll(new_course)
        @courses.each do |course|
            if course.conflicts_with?(new_course)
                raise 'There is a conflict for this course'
            end
        end
        if !@courses.include?(new_course)
            @courses << new_course
        end
        new_course.students << self
    end

    def course_load
        result = {}
        @courses.each do |course|
            if result[course.department]
                result[course.department] += course.credits
            else
                result[course.department] = course.credits
            end
        end
        result
    end
end
